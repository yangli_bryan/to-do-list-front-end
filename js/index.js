var originalText;

function addListItem() {
    var allItems = getAllItems();
    
    var listItem = document.createElement("li");
    var toDo = document.getElementById("input-box").value;
    toDo = toDo === null ? " " : toDo;
    listItem.innerHTML = '<input type="checkbox" onclick = "checkItem(this)">\n' + 
                         '<p ondblclick="makeEditable(this)" class="active"' +
                         'onkeypress="hitEnterAndModify(event, this)">' + toDo + '</p>' +
                        '\n<button id="delete-icon" onclick = "removeItem(this)"></button>';
    listItem.setAttribute("id", allItems.length);
    
    allItems.push({"index": allItems.length + 1, "itemContent": toDo,  "status": "active"});
    saveAllItems(allItems);
    document.getElementById("actual-list").prepend(listItem);
    document.getElementById("input-box").value = "";
    document.getElementsByClassName("footer-button-selected")[0].click();
}

function hitEnterAndAdd(event) {
    if (event.keyCode === 13) {
        addListItem();
    }
}

function makeEditable(elem) {
    elem.contentEditable = true;
}

function hitEnterAndModify(event, elem) {
    if (event.keyCode === 13) {
        elem.contentEditable = false;
        let index = elem.getAttribute("id");
        let allItems = getAllItems();
        itemToModify = allItems.find((it) => {
            return it.index == index});
        itemToModify.itemContent = elem.innerText;
        saveAllItems(allItems);
    }
}

function checkItem(elem) {
    var textNode = elem.parentNode.getElementsByTagName('p').item(0);
    let allItems =  getAllItems();
    let item = allItems.find((it) => {
                            return it.itemContent === textNode.innerHTML});
    if (elem.checked === true) {
        elem.parentNode.classList.remove("active");
        textNode.classList.remove("active");
        elem.parentNode.classList.add("checked");
        textNode.classList.add("checked");
        item.status = "checked";
    } else {
        elem.parentNode.classList.remove("checked");
        textNode.classList.remove("checked");
        elem.parentNode.classList.add("active");
        textNode.classList.add("active");
        item.status = "active";
    }
    saveAllItems(allItems);
    document.getElementsByClassName("footer-button-selected")[0].click();
}

function removeItem(elem) {
    var parentItem = elem.parentNode;
    var toRemove = parentItem.getElementsByTagName('p').item(0).innerHTML;
    parentItem.parentNode.removeChild(parentItem);
    var allItems = getAllItems().filter(item => item.itemContent !== toRemove);
    var itemContents = allItems.map(item => item.itemContent);
    allItems.forEach(item => item.index = itemContents.indexOf(item.itemContent));
    saveAllItems(allItems);
}

function reconstructOl() {
    var ol = document.getElementById('actual-list');
    ol.parentNode.removeChild(ol);
    
    document.getElementById('todo-list-block').appendChild(document.createElement('ol'));
    var newOl = document.getElementsByTagName('ol').item(0);
    newOl.id = "actual-list";
}

function addAllItems(items) {
    for (let i = 0; i < items.length; i++) {
        var listItem = document.createElement("li");
        listItem.innerHTML = '<input type="checkbox" onclick = "checkItem(this)">\n' + 
                             '<p ondblclick= "makeEditable(this)" onkeypress="hitEnterAndModify(event, this)">' 
                             + items[i].itemContent + '</p>' +
                             '\n<button id="delete-icon" onclick = "removeItem(this)"></button>';
        listItem.classList.add(items[i].status);
        listItem.getElementsByTagName('p').item(0).setAttribute("id", items[i].index);
        if (items[i].status === "checked") {
            listItem.firstElementChild.checked = true;
        }
        listItem.getElementsByTagName("p").item(0).classList.add(items[i].status);
        document.getElementById("actual-list").prepend(listItem);
    }
}

function showCompleted() {
    reconstructOl();
    var completedItems = getAllItems().filter(item => item.status === "checked");
    addAllItems(completedItems);
    var footerButtons = document.getElementsByName("footerButton");
    footerButtons.item(2).className = "footer-button-selected";
    footerButtons.item(0).className = "footer-button";
    footerButtons.item(1).className = "footer-button";
}

function showAll() {
    reconstructOl();
    var allItems = getAllItems();
    addAllItems(allItems);
    var footerButtons = document.getElementsByName("footerButton");
    footerButtons.item(0).className = "footer-button-selected";
    footerButtons.item(1).className = "footer-button";
    footerButtons.item(2).className = "footer-button";
}

function showToDo() {
    reconstructOl();
    var notDoneItems = getAllItems().filter(item => item.status !== "checked");
    addAllItems(notDoneItems);
    var footerButtons = document.getElementsByName("footerButton");
    footerButtons.item(1).className = "footer-button-selected";
    footerButtons.item(0).className = "footer-button";
    footerButtons.item(2).className = "footer-button";
}

function makeEditable(elem) {
    elem.contentEditable = true;
}

window.onload = function() {
    this.showAll();
}